/*
 * This file is part of the SavaPage project <http://savapage.org>.
 * Copyright (c) 2011-2014 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.client;

/**
 * The VM arguments of the {@link ClientApp}.
 * <p>
 * These are set in the Linux/Mac/Cygwin and Windows start scripts as generated
 * by the Maven {@code appassembler-maven-plugin}.
 * </p>
 *
 * @author Datraverse B.V.
 *
 */
public final class ClientAppVmArgs {

    /**
     * {@code -Dapp.name=name}.
     */
    private final String appName;

    /**
     * {@code -Dapp.pid=pid}.
     */
    private final String appPid;

    /**
     * {@code -Dapp.repo=repo}.
     */
    private final String appRepo;

    /**
     * {@code -Dapp.home=home}.
     */
    private final String appHome;

    public ClientAppVmArgs() {
        this.appName = System.getProperty("app.name");
        this.appPid = System.getProperty("app.pid");
        this.appRepo = System.getProperty("app.repo");
        this.appHome = System.getProperty("app.home");
    }

    public String getAppName() {
        return appName;
    }

    public String getAppPid() {
        return appPid;
    }

    public String getAppRepo() {
        return appRepo;
    }

    public String getAppHome() {
        return appHome;
    }

}
