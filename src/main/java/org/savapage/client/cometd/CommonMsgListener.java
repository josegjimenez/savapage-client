/*
 * This file is part of the SavaPage project <http://savapage.org>.
 * Copyright (c) 2011-2016 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.client.cometd;

/**
 *
 * @author Rijk Ravestein
 *
 */
public interface CommonMsgListener {

    void onInitialize(boolean isSuccessful);

    void onConnectionClosed();

    void onConnectionEstablished();

    void onConnectionBroken();

    void onPollInvitation(final Long prevMsgTime);

    void onAccountMessage(String message);

    void onJobTicketMessage(String message);

    void onPrintIn();

    void onPrintInExpired(String message);

    void onPrintOutMessage(String message);

    void onError(String message);

}
